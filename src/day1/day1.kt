package day1

import java.io.File

fun calculateFuel(mass: Int) = (mass / 3) - 2

tailrec fun calculateFuelOfFuel(mass: Int) : Int {
    val fuel = calculateFuel(mass)
    return when  {
        fuel < 0 -> 0
        else -> fuel + calculateFuel(fuel)
    }
}

fun main() {
    val masses = File("D:\\Entwicklung\\Kotlin\\adventofcode2019\\input\\day1.txt").readText()
        .split(Regex("\\s+"))
        .filter { it.isNotBlank() }
        .map { it.toInt() }
    println("Required fuel for modules is ${masses.map { calculateFuel(it) }.sum()}")
    println("Required fuel for modules and fuel is ${masses.map { calculateFuelOfFuel(it) }.sum()}")
}
