package day5

import intcode.Program
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File

fun main() {
    val origProgram = File("D:\\Entwicklung\\Kotlin\\adventofcode2019\\input\\day5.txt").readText()
//    val origProgram = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"
        .trim()
        .split(",")
        .filter { it.isNotBlank() }
        .map { it.toInt() }
        .toIntArray()

    val program = Program("p", origProgram.copyOf(), mutableListOf(5))
    runBlocking {
        launch { program.run() }
    }
    program.outputs.forEach { println(it) }
}
