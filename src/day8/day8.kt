package day8

import java.io.File

fun main() {
    val layers = File("D:\\Entwicklung\\Kotlin\\adventofcode2019\\input\\day8.txt").readText()
        .chunked(25 * 6)
        .filter { it.isNotBlank() }

    val fewestZeroLayer = layers.minBy { it.count { it == '0' } }
    val ones = fewestZeroLayer?.count { it == '1' } ?: 0
    val twos = fewestZeroLayer?.count { it == '2' } ?: 0
    println(ones * twos)

//    layers.forEach { println(it) }

    val resultLayer: StringBuilder = java.lang.StringBuilder("2".repeat(25 * 6))
    for (layer in layers) {
        for ((i, c) in layer.withIndex()) {
            if (c == '2') continue
            if (resultLayer[i] != '2') continue
            resultLayer[i] = c
        }
    }
    resultLayer.chunked(25).forEach { println(it) }
}