package day6

import java.io.File
import kotlin.math.min

fun orbitPath(orbitMap: Map<String, String>, obj: String) : List<String> = when {
    orbitMap.contains(obj) -> {
        val next = orbitMap.getOrDefault(obj, "")
        orbitPath(orbitMap, next) + next
    }
    else -> emptyList()
}

fun main() {
    val input = File("D:\\Entwicklung\\Kotlin\\adventofcode2019\\input\\day6_example2.txt").readLines()
        .map { it.trim() }
        .filter { it.isNotBlank() }
        .map { it.split(")") }
        .associateBy({ it[1] }, { it[0] })
//    println(input)

    val totalOrbitCount = input.keys
        .map { orbitPath(input, it).size }
        .sum()
    println(totalOrbitCount)

    val youPath = orbitPath(input, "YOU")
    val sanPath = orbitPath(input, "SAN")
    println(youPath)
    println(sanPath)
    var equalCount = 0
    for (i in 0 until min(youPath.size, sanPath.size)) {
        if (youPath[i] == sanPath[i]) {
            equalCount++
        }
    }
    val transferCount = youPath.size + sanPath.size - 2 * equalCount
    println(transferCount)
}