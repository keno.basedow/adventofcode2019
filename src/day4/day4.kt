package day4

fun valid(pass: String) : Boolean {
    var lastDigit = '0'
    val digitCount = mutableMapOf<Char, Int>()
    for (digit in pass) {
        when {
            digit < lastDigit -> return false
            digit > lastDigit -> digitCount[digit] = 1
            else -> digitCount[digit] = digitCount.getOrDefault(digit, 0) + 1
        }
        lastDigit = digit
    }
    return digitCount.values.contains(2)
}

fun main() {
    val validPassCount = (382345..843167)
        .map { it.toString() }
        .count { valid(it) }
    println("Number of day4.valid passwords is $validPassCount")
}