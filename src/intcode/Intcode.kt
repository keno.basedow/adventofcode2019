package intcode

import kotlinx.coroutines.yield

abstract class Operation(private val mode1: Int, private val mode2: Int) {
    companion object {
        const val POSITION_MODE = 0
        const val IMMEDIATE_MODE = 1
    }
    constructor() : this(0, 0)
    constructor(mode: Int) : this(mode, 0)
    fun get(program: IntArray, pos: Int, mode: Int) = when (mode) {
        POSITION_MODE -> program[program[pos]]
        IMMEDIATE_MODE -> program[pos]
        else -> throw IllegalArgumentException("Invalid parameter mode $mode")
    }
    fun set(program: IntArray, pos: Int, value: Int) {
        program[program[pos]] = value
    }
    fun one(program: IntArray, pc: Int) = get(program, pc+1, mode1)
    fun two(program: IntArray, pc: Int) = get(program, pc+2, mode2)
    abstract suspend fun execute(program: IntArray, pc: Int) : Int
}

class Add(mode1: Int, mode2: Int) : Operation(mode1, mode2) {
    override suspend fun execute(program: IntArray, pc: Int): Int {
//        println("#${program[pc+3]} = ${one(program, pc)} + ${two(program, pc)}")
        set(program, pc+3,one(program, pc) + two(program, pc))
        return pc+4
    }
}

class Multiply(mode1: Int, mode2: Int) : Operation(mode1, mode2) {
    override suspend fun execute(program: IntArray, pc: Int): Int {
//        println("#${program[pc+3]} = ${one(program, pc)} * ${two(program, pc)}")
        set(program, pc+3,one(program, pc) * two(program, pc))
        return pc+4
    }
}

class Input(private val inputs: MutableList<Int>) : Operation() {
    override suspend fun execute(program: IntArray, pc: Int): Int {
        while (inputs.isEmpty())
            yield()
        val input = inputs.removeAt(0)
//        println("#${program[pc+1]} = Input $input")
        set(program, pc+1, input)
        return pc+2
    }
}

class Output(mode: Int, private val outputs: MutableList<Int>) : Operation(mode) {
    override suspend fun execute(program: IntArray, pc: Int): Int {
//        println("Output ${one(program, pc)}")
        outputs += one(program, pc)
        return pc+2
    }
}

class JumpIfTrue(mode1: Int, mode2: Int) : Operation(mode1, mode2) {
    override suspend fun execute(program: IntArray, pc: Int): Int {
        return when {
            one(program, pc) != 0 -> {
//                println("Jump True ${two(program, pc)}")
                two(program, pc)
            }
            else -> pc + 3
        }
    }
}

class JumpIfFalse(mode1: Int, mode2: Int) : Operation(mode1, mode2) {
    override suspend fun execute(program: IntArray, pc: Int): Int {
        return when {
            one(program, pc) == 0 -> {
//                println("Jump False ${two(program, pc)}")
                two(program, pc)
            }
            else -> pc + 3
        }
    }
}

class LessThen(mode1: Int, mode2: Int) : Operation(mode1, mode2) {
    override suspend fun execute(program: IntArray, pc: Int): Int {
        val result = if (one(program, pc) < two(program, pc)) 1 else 0
//        println("#${program[pc+3]} = $result because ${one(program, pc)} < ${two(program, pc)}")
        set(program, pc+3, result)
        return pc+4
    }
}

class Equals(mode1: Int, mode2: Int) : Operation(mode1, mode2) {
    override suspend fun execute(program: IntArray, pc: Int): Int {
        val result = if (one(program, pc) == two(program, pc)) 1 else 0
//        println("#${program[pc+3]} = $result because ${one(program, pc)} == ${two(program, pc)}")
        set(program, pc+3, result)
        return pc+4
    }
}

class Program(private val name: String, private val programCode: IntArray,
              var inputs: MutableList<Int>, var outputs: MutableList<Int> = mutableListOf<Int>()) {

    private var pc = 0

    companion object {
        const val ADD = 1
        const val MULTIPLY = 2
        const val INPUT = 3
        const val OUTPUT = 4
        const val JUMP_IF_TRUE = 5
        const val JUMP_IF_FALSE = 6
        const val LESS_THEN = 7
        const val EQUALS = 8
        const val TERMINATE = 99
    }

    private fun extractOperation(inputs: MutableList<Int>, outputs: MutableList<Int>) : Operation {
        val code = programCode[pc]
        val opcode = code % 100
        val mode1 = (code / 100) % 10
        val mode2 = (code / 1000) % 10
        return when(opcode) {
            ADD -> Add(mode1, mode2)
            MULTIPLY -> Multiply(mode1, mode2)
            INPUT -> Input(inputs)
            OUTPUT -> Output(mode1, outputs)
            JUMP_IF_FALSE -> JumpIfFalse(mode1, mode2)
            JUMP_IF_TRUE -> JumpIfTrue(mode1, mode2)
            LESS_THEN -> LessThen(mode1, mode2)
            EQUALS -> Equals(mode1, mode2)
            else -> throw IllegalArgumentException("Invalid opcode $opcode")
        }
    }

    suspend fun run() {
        pc = 0
        while (programCode[pc] != TERMINATE)
        {
            val operation = extractOperation(inputs, outputs)
//            print("$name: ")
            pc = operation.execute(programCode, pc)
        }
    }
}
