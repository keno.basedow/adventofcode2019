package day2

import java.io.File
import java.lang.IllegalArgumentException

const val ADD = 1
const val MUL = 2
const val TER = 99

fun operation(program: IntArray, pc: Int, operation: (Int, Int) -> Int) : Int {
    val pos1 = program[pc + 1]
    val pos2 = program[pc + 2]
    val posR = program[pc + 3]
    program[posR] = operation(program[pos1], program[pos2])
    return pc + 4
}

fun runProgram(program: IntArray, noun: Int, verb: Int) {
    program[1] = noun
    program[2] = verb

    var pc = 0
    while (program[pc] != TER)
    {
        when (program[pc]) {
            ADD -> pc = operation(program, pc) { a, b -> a + b }
            MUL -> pc = operation(program, pc) { a, b -> a * b }
            TER -> return
            else -> throw IllegalArgumentException("Unknown opcode ${program[pc]} at position $pc")
        }
    }
}

fun main() {
    val origProgram = File("D:\\Entwicklung\\Kotlin\\adventofcode2019\\input\\day2.txt").readText()
        .trim()
        .split(",")
        .filter { it.isNotBlank() }
        .map { it.toInt() }
        .toIntArray()

    val program1 = origProgram.copyOf()
    val noun1 = 12
    val verb1 = 2
    runProgram(program1, noun1, verb1)
    println("Result for noun $noun1 and verb $verb1 is ${program1[0]}")

    val expected = 19690720
    for (noun in 0..99) {
        for (verb in 0..99) {
            val program =  origProgram.copyOf()
            runProgram(program, noun, verb)
            if (program[0] == expected)
                println("Expected value $expected is produced by noun $noun and verb $verb: Result is ${100 * noun + verb}")
        }
    }
}
