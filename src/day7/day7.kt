package day7

import intcode.Program
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.IllegalArgumentException

fun runAmplifiers(programCode: IntArray, phaseSettings: IntArray) : Int {
    if (phaseSettings.size < 5)
        throw IllegalArgumentException("must have 5 phase settings")

    val p1 = Program("1", programCode.copyOf(), mutableListOf(phaseSettings[0], 0))
    p1.outputs.add(phaseSettings[1])
    val p2 = Program("2", programCode.copyOf(), p1.outputs)
    p2.outputs.add(phaseSettings[2])
    val p3 = Program("3", programCode.copyOf(), p2.outputs)
    p3.outputs.add(phaseSettings[3])
    val p4 = Program("4", programCode.copyOf(), p3.outputs)
    p4.outputs.add(phaseSettings[4])
    val p5 = Program("5", programCode.copyOf(), p4.outputs, p1.inputs)

    runBlocking {
        launch { p1.run() }
        launch { p2.run() }
        launch { p3.run() }
        launch { p4.run() }
        launch { p5.run() }
    }

    return p5.outputs[0]
}

fun runAmplifiersSync(programCode: IntArray, phaseSettings: IntArray) : Int {
    var output = 0
    for (phaseSetting in phaseSettings) {
        val program = Program("p", programCode.copyOf(), mutableListOf(phaseSetting, output))
        runBlocking {
            launch { program.run() }
        }
        output = program.outputs[0]
    }
    return output
}

fun main() {
    val program = File("D:\\Entwicklung\\Kotlin\\adventofcode2019\\input\\day7.txt").readText()
        .trim()
        .split(",")
        .filter { it.isNotBlank() }
        .map { it.toInt() }
        .toIntArray()

    var highestSignal = 0
    val phases = 5..9
    for (p1 in phases) {
        for (p2 in (phases - p1)) {
            for (p3 in (phases - p1 - p2)) {
                for (p4 in (phases - p1 - p2 - p3)) {
                    for (p5 in (phases - p1 - p2 - p3 - p4)) {
                        val signal = runAmplifiers(program, intArrayOf(p1, p2, p3, p4, p5))
                        if (signal > highestSignal)
                            highestSignal = signal
                    }
                }
            }
        }
    }
    println(highestSignal)
}