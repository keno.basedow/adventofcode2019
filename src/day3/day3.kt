package day3

import java.io.File
import java.lang.IllegalArgumentException
import kotlin.math.abs

data class Position(val x: Int, val y: Int) {
    override fun toString() = "($x,$y)"
    fun distance() = abs(x) + abs(y)
}

data class Line(val direction: Char, val length: Int)

fun convertPath(instr : String) = instr.split(",")
    .map { Line(it[0], it.substring(1).toInt()) }

fun linePositions(p: Position, l: Line) = when (l.direction) {
    'R' -> Array(l.length) { Position(p.x + it + 1, p.y) }
    'L' -> Array(l.length) { Position(p.x - it - 1, p.y) }
    'U' -> Array(l.length) { Position(p.x, p.y + it + 1) }
    'D' -> Array(l.length) { Position(p.x, p.y - it - 1) }
    else -> throw IllegalArgumentException("Unknown direction ${l.direction}")
}.toList()

fun collectPositions(path : List<Line>) : List<Position> {
    var position = Position(0, 0)
    val positions = mutableListOf<Position>()
    for (line in path) {
        positions += linePositions(position, line)
        position = positions.last()
    }
    return positions
}

fun smallestDistance(positions: Set<Position>) = positions.map { it.distance() }.min()

fun shortestPaths(positions1: List<Position>, positions2: List<Position>, crossPositions: Set<Position>)
    = crossPositions.map { positions1.indexOf(it)+1 + positions2.indexOf(it)+1 }.min()

fun main() {

//    val input = arrayOf("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
//    val input = arrayOf("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83")
//    val input = arrayOf("R8,U5,L5,D3", "U7,R6,D4,L4")
    val input = File("D:\\Entwicklung\\Kotlin\\adventofcode2019\\input\\day3.txt").readLines()
    val path1 = convertPath(input[0])
    val path2 = convertPath(input[1])
    val positions1 = collectPositions(path1)
    val positions2 = collectPositions(path2)
    val crossPositions = positions1.intersect(positions2)
    val distance = smallestDistance(crossPositions)
    println("Smallest distance to crossing paths is $distance")
    val shortest = shortestPaths(positions1, positions2, crossPositions)
    println("Shortest paths to crossing paths is $shortest")
}